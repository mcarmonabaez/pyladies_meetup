R en producción
===
author: Mariana Carmona Baez
date: 27 agosto, 2019
width: 960
height: 700
transition: linear
css: rpres.css

<!-- NOTE: Styling and external images may be missing --> 
<p>
  <br/>
  Data Science Manager, Global Innovations Team
  <br/>
  Kantar
</p>
<img src="www/rladies_logo-02.png" height="130px" width="128px" />


Contexto
========================================================


__Beast__ (Brand Equity Analytical System Tools) es una plataforma para operar servicios de análisis a gran escala.

-	Multi – ambiente (Lab, Dev, Test, UAT, Prod)
-	Escalable y robusto
-	Acceso via web para uso global
-	Alojado en Microsoft Azure Platform
-	Soporte para __R__ y __Python__

<div style="text-align: right"><img src="www/beast.PNG" height="140px" width="138px" />


Ecosistema Azure
========================================================

__Azure__: servicios en la nube de Microsoft

- _Blob storage_ y conexiones a bases de datos para archivos de entrada y almacenamiento
- Integración con aplicaciones de terceros (SendGrid)
- Integración con otras herramientas de Azure (_Azure Batch_, _Azure Functions_, etc.)
- Seguro (service level agreement)

<div style="text-align: right"><img src="www/Azure.png" height="130px" width="128px" />


Azure DevOps (kantarware)
========================================================

Kantarware es una instancia de Azure DevOps (antes Microsoft's Visual Studio Online) que nos permite tener:

- Repositorios __Git__ ilimitados
- Fácil seguimiento del trabajo utilizando herramientas de _agile_ (backlogs, manejo de sprints, tableros por equipo)
- Control de acceso al código (active directory)
- Accesibilidad a librerías de R y Python desarrolladas (democratización del código)
- Pipelines de automatización de la instalación de paquetes en el servidor de la Bestia (_push_, _build_, _release_)




Agile
========================================================

<center><img src="www/agile_beast.png" height="600px" /><center>


Lanzamiento a Producción
========================================================

- Liberación de código __planeada__. 


<center><img src="www/prod.jpeg" height="530px" width="500px" /><center>



Debugging
========================================================

_Debuggear_ es un trabajo de detective. Más aún si tienes dependencias entre paquetes o tu paquete incluye una Shiny app, porque la ejecución del código no es lineal.


<center><img src="www/disturbance.jpeg" height="350px" width="450px" /><center>




Debugging
========================================================

- **Debugging**: Detener la ejecución del programa en algún lugar en particular. Sirve para revisar cada paso conforme lo vamos ejecutando. Es útil cuando sabemos la sección del código en donde está el error (_breakpoints_ y `browser()`)

 

- **Tracing**: Recolectar información conforme el programa corre para después analizarlo. En este caso no hay pausas en la ejecución. Por ejemplo, revisar algunas estadísticas resumen de algún modelo
  

- **Error handling**: Pensar en varias fuentes de error e identificar la causa. Se utlizan bloques try y catch. Lo más importante es que la ejecución no se detenga


Aprendizajes
========================================================


- __¡comentarios!__
- trabajo interdisciplinario
- paquetes: devtools (lint), data.table y magrittr
- Versión de R y checkpoint
- shortcuts de RStudio para desarrollo de paquetes






