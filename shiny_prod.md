Shiny en producción
===
author: Karen González Contreras
date: 27 agosto, 2019
width: 960
height: 700
transition: linear
css: rpres.css

<!-- NOTE: Styling and external images may be missing --> 
<p>
  <br/>
  Data Scientist, Global Innovations Team
  <br/>
  Kantar
</p>
<img src="www/rladies_logo-02.png" height="130px" width="128px" />


Principales funciones
========================================================

Desarrollo y mantenimiento de las aplicaciones para la integración al __Holistic Brand Guidance__ (HBG) de Kantar.

HBG es una nueva iniciativa enfocada en proporcionar un análisis rápido y eficiente, con el objetivo de tener un mayor entendimiento de las marcas y sus principales métricas.

- ¿Cómo se encuentra la marca con respecto a su categoría?
- Seguimiento de las métricas a lo largo del tiempo
- NLP para entender el contexto de una marca

Infraestructura y Software
========================================================

- Azure
- Kantarware (Azure DevOps)
- Shiny Server
- Docker 
- R y Python

<div style="text-align: right"><img src="www/docker.png" height="100px" />

Flujo de trabajo en producción
========================================================

<img src="www/agile.png" height="600px" />


Estructura de una app
========================================================

<center><img src="www/estructura_completa.PNG" height="600px" /><center>


Infraestructura en producción
========================================================

<center><img src="www/TrendAI_Dashboard.png" height="600px" /><center>

Aprendizajes
========================================================

- Comentarios y documentación
- Usar config files
- Empaquetar  las apps
- Trabajar con el checkpoint correcto de R
- Usar funciones
- Separar la app en componentes
- Tener presente el feedback de tu equipo

<center><img src="www/cute_r.png" height="100px" /><center>
